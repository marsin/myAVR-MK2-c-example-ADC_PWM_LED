/* myAVR - C example - ADC, PWM, LED
 * =================================
 *
 *  A simple C example, dimming a LED by PWM, reading an ADC value,
 *    using an ATmega8L controller.
 */


/** Main program.
 *
 * @file   main.c
 * @date   2019
 * @author Martin Singer
 */

#ifndef F_CPU
  #define F_CPU 3686400UL
#endif

#define DELAY 5.0       // proposed delay time for a fading value in ms


#include <avr/io.h>
#include <util/delay.h> // requires F_CPU


/** Fade LED up. */
void led_fade_up(void)
{
	for (int i = 0x00; i < 0xFF; ++i) {
		OCR2 = i;
		_delay_ms(DELAY);
	}
	return;
}


/** Fade LED down. */
void led_fade_down(void)
{
	for (int i = 0xFF; i > 0x00; --i) {
		OCR2 = i;
		_delay_ms(DELAY);
	}
	return;
}


/** ADC read.
 *
 * Reading the specific ADC channel.
 *
 * @param cannel   ADC channel number
 * @return         ADC readout value
 */
uint16_t adc_read(uint8_t channel)
{
	ADMUX = (ADMUX & ~(0x1F)) | (channel & 0x1F); // select ADC channel
	ADCSRA |= (1<<ADSC);                          // start single AD conversion
	while (ADCSRA & (1<<ADSC)) {}                 // wait for AD conversion
	return ADCW;                                  // return ADCL and ADCH readout
}


/** Average ADC read value of a specific number of samples.
 *
 * Reading the specific ADC channel
 * and calculating an average value from the amount of samples.
 *
 * @see    adc_read()
 * @param  cannel   ADC channel number
 * @param  nsamples amount of samples to read
 * @return          average ADC readout value
 */
uint16_t adc_read_avg(uint8_t channel, uint8_t nsamples)
{
	uint32_t sum = 0x00000000;

	for (uint8_t i = nsamples; i > 0; --i) {
		sum += adc_read(channel);
	}

	return (uint16_t) (sum / (uint32_t) nsamples);
}


/** Main function. */
int main(void)
{
	// Init PWM
	DDRB |= (1<<DDB3);                // set port PB3 as output
	OCR2 = 0x00;                      // set PWM for 00% duty cycle
	TCCR2 |= (1<<COM21);              // set none-inverting mode
	TCCR2 |= (1<<WGM21) | (1<<WGM20); // set fast PWM mode
	TCCR2 |= (1<<CS21);               // set prescaler to 8 and start PWM

	// Init ADC
	ADMUX = (1<<REFS0);               // set AVcc with external capacitor at AREF pin
	ADCSRA = (1<<ADPS1) | (1<<ADPS0); // set prescaler to 8
	ADCSRA = (1<<ADEN);               // start ADC

	// ADC dummy readout (first readout after init)
	ADCSRA |= (1<<ADSC);              // start single AD conversion
	while (ADCSRA & (1<<ADSC)) {}     // wait for AD conversion
	(void) ADCW;                      // discard ADCL and ADCH readout (required for next AD conversion)


	uint16_t adcvalue = 0x0000;

	do {
//		led_fade_up();
//		led_fade_down();
		adcvalue = adc_read(0);
		OCR2 = (uint8_t) (adcvalue / 4.0);
	} while (1);

	return 0;
}

