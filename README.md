myAVR-MK2 - C example - ADC, PWM, LED
=====================================

 A simple C example, dimming a LED by PWM, reading an ADC value,
   using an ATmega8L controller.


[Datasheet]: https://www.microchip.com/wwwproducts/en/ATmega8 "ATmega8(L) Datasheet"
[Tutorial]: https://www.mikrocontroller.net/articles/AVR-GCC-Tutorial/Analoge_Ein-_und_Ausgabe "AVR-GCC-Tutorial/Analoge Ein- und Ausgabe – Mikrocontroller.net"
[myAVR-MK2-c-example-PWM_LED]: https://gitlab.com/marsin/myAVR-MK2-c-example-PWM_LED
[Board]: http://shop.myavr.com/?sp=article.sp.php&artID=40 "myAVR MK2 board"
[Board Manual]: http://shop.myavr.com/index.php?sp=download.sp.php&suchwort=dl39 "myAVR MK2 board manual"


References
----------

 - ATmega8(L) - Complete [Datasheet], Rev. **2486AA-AVR-02/2013**
 - AVR-GCC-[Tutorial]/Analoge Ein- und Ausgabe – Mikrocontroller.net
 - For information about PWM LED dimming,
   have a look at the [myAVR-MK2-c-example-PWM_LED] example
 - The myAVR MK2 [Board]
 - The myAVR MK2 [Board Manual]


Pin Configuration
-----------------

### PDIP

 - *[Datasheet], Page 2*
 - *[Datasheet], Page 196*
 - *[Board Manual], Page 6*


```
       ---xxx---
PC6 --| 1     28|-- PC5 (ADC5)        GND ------[Rvar]------ VCC
PD0 --| 2     27|-- PC4 (ADC4)                    ^
PD1 --| 3     26|-- PC3 (ADC3)   ,----------------'
PD2 --| 4  A  25|-- PC2 (ADC2)   |
PD3 --| 5  T  24|-- PC1 (ADC1)   |           200nF    10uH
PD4 --| 6  m  23|-- PC0 (ADC0) --'    GND --+--||--+--^^^^-- VCC
VCC --| 7  e  22|-- AGND -------------------'      |
GND --| 8  g  21|-- AREF --------,                 |
PB6 --| 9  a  20|-- AVCC --------+-----------------'
PB7 --|10  8  19|-- PB5
PD5 --|11  L  18|-- PB4
PD6 --|12     17|-- PB3 (OC2) ---------------[ R ]---LED>|-- GND
PD7 --|13     16|-- PB2
PB0 --|14     15|-- PB1
       ---------
```


Registers
---------

### **ADCSRA**: ADC Control and Status Register A

 *[Datasheet], Page 200*


 |     7 |     6 |     5 |     4 |     3 |     2 |     1 |     0 |
 | ----: | ----: | ----: | ----: | ----: | ----: | ----: | ----: |
 |  ADEN |  ADSC |  ADFR |  ADIF |  ADIE | ADPS2 | ADPS1 | ADPS0 |


 - *Bit 7*    - **ADEN**:     ADC Enable
 - *Bit 6*    - **ADSC**:     ADC Start Conversion
 - *Bit 5*    - **ADFR**:     ADC Free Running Select
 - *Bit 4*    - **ADIF**:     ADC Interrupt Flag
 - *Bit 3*    - **ADIE**:     ADC Interrupt Enable
 - *Bit 2..0* - **ADPS2..0**: ADC Prescaler Select Bits


 | ADPS2 | ADPS1 | ADPS0 | Division Factor |
 | ----: | ----: | ----: | :-------------- |
 |     0 |     0 |     0 | 2               |
 |     0 |     0 |     1 | 2               |
 |     0 |     1 |     0 | 4               |
 |     0 |     1 |     1 | 8               |
 |     1 |     0 |     0 | 16              |
 |     1 |     0 |     1 | 32              |
 |     1 |     1 |     0 | 64              |
 |     1 |     1 |     1 | 128             |


### **ADMUX**: ADC Multiplexer Select Register

 *[Datasheet], Page 199*


 |     7 |     6 |     5 |     4 |     3 |     2 |     1 |     0 |
 | ----: | ----: | ----: | ----: | ----: | ----: | ----: | ----: |
 | REFS1 | REFS0 | ADLAR |     - |  MUX3 |  MUX2 |  MUX1 |  MUX0 |


 - *Bit 7:6*  - **REFS1:0**: Reference Selection Bits
 - *Bit 5*    - **ADLAR**:   ADC Left Adjust Result
 - *Bit 3..0* - **MUX3..0**: Analog Chanel Selection Bits


 | REFS1 | REFS0 | Voltage Reference Selection                                          |
 | ----: | ----: | :------------------------------------------------------------------- |
 |     0 |     0 | AREF, Internal Vref turned off                                       |
 |     0 |     1 | AVcc with external capacitor at AREF pin                             |
 |     1 |     0 | Reserved                                                             |
 |     1 |     1 | Internal 2.56V Voltage Reference with external capacitor at AREF pin |


 | MUX3..0 | Single Ended Input |
 | ------: | :----------------- |
 |    0000 | ADC0               |
 |    0001 | ADC1               |
 |    0010 | ADC2               |
 |    0011 | ADC3               |
 |    0100 | ADC4               |
 |    0101 | ADC5               |
 |    0110 | ADC6               |
 |    0111 | ADC7               |
 |    1000 | -                  |
 |    1001 | -                  |
 |    1010 | -                  |
 |    1011 | -                  |
 |    1100 | -                  |
 |    1101 | -                  |
 |    1110 | 1.30V (VBG)        |
 |    0111 | 0V (GND)           |


### **ADCL**, **ADCH**: The ADC Data Register (Low, High)

 *[Datasheet], Page 201*


 When an ADC conversion is complete, the result is found in these two registers.

 Example reading the registers:

```c
uint16_t value = 0x00;

value  = ADCL;        // LSB first
value += (ADCH << 8); // MSB second

// or

value = ADCW;         // reads ADCL and ADCH
```

#### ADLAR = 0

 *[Datasheet], Page 201*


**ADCL**:

 |     7 |     6 |     5 |     4 |     3 |     2 |     1 |     0 |
 | ----: | ----: | ----: | ----: | ----: | ----: | ----: | ----: |
 |  ADC7 |  ADC6 |  ADC5 |  ADC4 |  ADC3 |  ADC2 |  ADC1 |  ADC0 |


**ADCH**

 |     7 |     6 |     5 |     4 |     3 |     2 |     1 |     0 |
 | ----: | ----: | ----: | ----: | ----: | ----: | ----: | ----: |
 |     - |     - |     - |     - |     - |     - |  ADC9 |  ADC8 |


